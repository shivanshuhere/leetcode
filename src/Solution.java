class Solution {
	public static void main(String[] args) {
		int nums[] = {-7,-3,2,3,11};
		int sortedNums[] = sortedSquares(nums);
		for(int i : sortedNums) {
			System.out.println("result : "+ i);
		}
	}
	public static int[] sortedSquares(int[] nums) {
		int arr[] = new int[nums.length];
		int first = 0, last = nums.length-1;
		for(int i=nums.length-1; i>=0; i--) {
			if(Math.abs(nums[first])>Math.abs(nums[last])) {
				arr[i] = Math.multiplyExact(nums[first], nums[first]);
				first++;
			}else{
				arr[i] = Math.multiplyExact(nums[last], nums[last]);
				last--;
			}
		}
		return arr;
	}
}